import Vue from "vue";
import Vuex from "vuex";
// import database from "./db/firebase";
import Core from "./sets/core-v1/cards";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    sets: [
      {
        Name: "core-v1",
        Cards: null
      }
    ],
  },
  mutations: {
    setCoreSet(state, payload) {
      state.sets[0].Cards = payload;
    }
  },
  actions: {
    loadCoreSet({ commit }) {
      commit("setCoreSet", Core);
    }
  }
});
