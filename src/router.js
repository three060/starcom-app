import Vue from "vue";
import Router from "vue-router";
import Collections from "./components/Collection/Collection.vue";
import Play from "./components/Play/Play.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "collections",
      component: Collections
    },
    {
      path: "/play",
      name: "play",
      component: Play
    }
  ]
});
