"use strict";

import { app, protocol, BrowserWindow, ipcMain } from "electron";
import * as path from "path";
import { format as formatUrl } from "url";
import createProtocol from "vue-cli-plugin-electron-builder/lib/createProtocol.js";

const isDevelopment = process.env.NODE_ENV !== "production";

// global reference to mainWindow (necessary to prevent window from being garbage collected)
let mainWindow;
let gameServer;

// Standard scheme must be registered before the app is ready
protocol.registerStandardSchemes(["app"], { secure: true });
function createMainWindow() {
  const window = new BrowserWindow({
    width: 1024,
    height: 720,
    fullscreen: true,
    autoHideMenuBar: true
  });

  if (isDevelopment) {
    // Load the url of the dev server if in development mode
    window.loadURL(process.env.WEBPACK_DEV_SERVER_URL);
    // if (!process.env.IS_TEST) window.webContents.openDevTools();
  } else {
    createProtocol("app");
    //   Load the index.html when not in development
    window.loadURL(
      formatUrl({
        pathname: path.join(__dirname, "index.html"),
        protocol: "file",
        slashes: true
      })
    );
  }

  window.on("closed", () => {
    mainWindow = null;
    if (gameServer !== null) gameServer.close();
  });

  window.webContents.on("devtools-opened", () => {
    window.focus();
    setImmediate(() => {
      window.focus();
    });
  });

  return window;
}

// For hosting a local network game
function createGameServer() {
  const window = new BrowserWindow({
    width: 400,
    height: 400,
    show: false
  });

  if (isDevelopment) {
    // Load the url of the dev server if in development mode
    window.loadURL(path.join(__dirname, "..", "public", "gameServer.html"));
    // if (!process.env.IS_TEST) window.webContents.openDevTools();
  } else {
    createProtocol("app");
    //   Load the server.html when not in development
    window.loadURL(
      formatUrl({
        pathname: path.join(__dirname, "gameServer.html"),
        protocol: "file",
        slashes: true
      })
    );
  }

  window.on("closed", () => {
    gameServer = null;
  });

  return window;
}

ipcMain.on("initGameServer", event => {
  gameServer = createGameServer();
  event.returnValue = true;
});

ipcMain.on("closeGameServer", event => {
  // Find a way to send final UDP message removing game from any lists.
  // Don't want closed games to be sticking around.
  gameServer.close();
  event.returnValue = true;
});

// quit application when all windows are closed
app.on("window-all-closed", () => {
  // on macOS it is common for applications to stay open until the user explicitly quits
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  // on macOS it is common to re-create a window even after all windows have been closed
  if (mainWindow === null) {
    mainWindow = createMainWindow();
  }
});

// create main BrowserWindow when electron is ready
app.on("ready", () => {
  mainWindow = createMainWindow();
});
