import firebase from "firebase";

const config = {
  apiKey: "AIzaSyDgqeBg6rMh4xAnqDWe5wwg8rAcCc23UD0",
  authDomain: "starcom-5377f.firebaseapp.com",
  databaseURL: "https://starcom-5377f.firebaseio.com",
  projectId: "starcom-5377f",
  storageBucket: "starcom-5377f.appspot.com",
  messagingSenderId: "794251228756"
};

firebase.initializeApp(config);

const database = firebase.database();

export default database;