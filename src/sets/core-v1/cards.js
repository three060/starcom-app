const Cards = [
  {
    ID: 1,
    Attack: "B1",
    Cost: "1P",
    Faction: "Drummond",
    Hull: 2,
    Name: "Discovery's Grace",
    Rules:
      "When this ship explores a new system, you may search your deck for a ship with logistics 2 or less, reveal it, and put it into your hand. Shuffle your deck.",
    Shield: 2,
    Type: "Fast Frigate",
    Image: "discoverys_grace.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 2,
    Attack: "B2",
    Cost: "2P",
    Faction: "Drummond",
    Hull: 2,
    Name: "Archer",
    Rules: "Long Range",
    Shield: 2,
    Type: "Fast Frigate",
    Image: "archer.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 3,
    Attack: "M2",
    Cost: "2P",
    Faction: "Drummond",
    Hull: 2,
    Name: "Longbow",
    Rules: "Long Range",
    Shield: 2,
    Type: "Frigate",
    Image: "longbow.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 4,
    Attack: "M3",
    Cost: "4P",
    Faction: "Drummond",
    Hull: 3,
    Name: "Trebuchet",
    Rules: "Long Range",
    Shield: 3,
    Type: "Cruiser",
    Image: "trebuchet.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 5,
    Attack: "B2",
    Cost: "1P",
    Faction: "Drummond",
    Hull: 2,
    Name: "Yeoman",
    Rules: "Initiative",
    Shield: 1,
    Type: "Fast Frigate",
    Image: "yeoman.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 6,
    Attack: "M2",
    Cost: "3P",
    Faction: "Drummond",
    Hull: 2,
    Name: "Glory's Arrow",
    Rules: "Initiative",
    Shield: 2,
    Type: "Frigate",
    Image: "glorys_arrow.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 8,
    Attack: "B0",
    Cost: "2P",
    Faction: "Drummond",
    Hull: 5,
    Name: "Repair Frigate",
    Rules: "2C: Repair 1 damage from target ship.",
    Shield: 1,
    Type: "Frigate",
    Image: "repair_frigate.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 9,
    Attack: "M1",
    Cost: "2P",
    Faction: "Drummond",
    Hull: 3,
    Name: "Hull Drainer",
    Rules: "Drain Hull",
    Shield: 2,
    Type: "Frigate",
    Image: "hull_drainer.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 11,
    Attack: "M2",
    Cost: "3P",
    Faction: "Drummond",
    Hull: 3,
    Name: "Salvage Frigate",
    Rules:
      "XC: Return a frigate with logistics X from your discard pile to your deck. Shuffle your deck.",
    Shield: 3,
    Type: "Frigate",
    Image: "salvage_frigate.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 13,
    Attack: "B2",
    Cost: "4P",
    Faction: "Drummond",
    Hull: 3,
    Name: "Salvage Cruiser",
    Rules:
      "XC: Return a frigate with logistics X from your discard pile to your deck. Shuffle your deck.",
    Shield: 3,
    Type: "Cruiser",
    Image: "salvage_cruiser.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 14,
    Attack: "B1",
    Cost: "4P",
    Faction: "Drummond",
    Hull: 5,
    Name: "Repair Cruiser",
    Rules:
      "During your end phase, you may repair 1 damage from up to three target ships. | 2C: Repair 1 damage from target ship.",
    Shield: 3,
    Type: "Cruiser",
    Image: "repair_cruiser.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 16,
    Attack: "M2",
    Cost: "4P",
    Faction: "Drummond",
    Hull: 12,
    Name: "Salvage Station",
    Rules:
      "XC: Return a ship with logistics X from your discard pile to your deck. Shuffle your deck.",
    Shield: 9,
    Type: "Station",
    Image: "salvage_station.jpg",
    HandContext: [
      {
        Text: "Build Station",
        Action: "build"
      }
    ],
    PlayContext: []
  },
  {
    ID: 17,
    Cost: "3C",
    Faction: "Drummond",
    Name: "Lead the Charge",
    Rules: "Target fleet is fast until end of turn.",
    Type: "Command",
    Image: "lead_the_charge.jpg"
  },
  {
    ID: 18,
    Cost: "2C",
    Faction: "Drummond",
    Name: "Barrage",
    Rules: "A ship you control deals 2 damage to target ship or station.",
    Type: "Maneuver",
    Image: "barrage.jpg"
  },
  {
    ID: 22,
    Cost: "3C",
    Faction: "Drummond",
    Name: "Let Starlight Blind",
    Rules:
      "Until your next turn, prevent all damage in target system. No ship can jump to or from that system.",
    Type: "Maneuver",
    Image: "let_starlight_blind.jpg"
  },
  {
    ID: 24,
    Cost: "3P",
    Faction: "Drummond",
    Name: "Advanced Missiles",
    Rules: "Ships you control with missiles get +1 attack",
    Type: "Research",
    Image: "advanced_missiles.jpg"
  },
  {
    ID: 26,
    Credits: 2,
    Faction: "Drummond",
    Loyalty: 4,
    Name: "Drummond",
    Production: 2,
    Type: "Capital Terran Planet",
    Image: "drummond.jpg",
    HandContext: [
      {
        Text: "Explore System",
        Action: "explore"
      }
    ]
  },
  {
    ID: 27,
    Credits: 1,
    Faction: "Drummond",
    Loyalty: 1,
    Name: "Refugee World",
    Production: 1,
    Type: "Terran Planet",
    Image: "refugee_world.jpg",
    HandContext: [
      {
        Text: "Explore System",
        Action: "explore"
      }
    ]
  },
  {
    ID: 28,
    Credits: 2,
    Faction: "Drummond",
    Loyalty: 2,
    Name: "Mining Colony",
    Production: 1,
    Type: "Volcanic Planet",
    Image: "mining_colony.jpg",
    HandContext: [
      {
        Text: "Explore System",
        Action: "explore"
      }
    ]
  },
  {
    ID: 29,
    Attack: "B1",
    Cost: "2P",
    Faction: "Sol",
    Flavor:
      '"Under my rule, the forces of the galaxy will be united in common cause to bring us into glory." - Admiral Rasik Setanden, SIF Blade\'s Edge',
    Hull: 3,
    Name: "Explorer",
    Rules: "When this ship explores a new system, gain +1 industry.",
    Shield: 3,
    Type: "Frigate",
    Image: "explorer.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 31,
    Attack: "B1",
    Cost: "4P",
    Faction: "Sol",
    Flavor:
      '"Why only build ships on worlds where they are not needed? Let us go to the sky and build them there." - Admiral Rasik Setenden, SIF Blade\'s Edge',
    Hull: 3,
    Name: "Sol Constructor",
    Rules: "Gain +1P per turn.",
    Shield: 3,
    Type: "Cruiser",
    Image: "sol_constructor.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 34,
    Attack: "B3",
    Cost: "4P",
    Faction: "Sol",
    Hull: 5,
    Name: "Interdictor",
    Rules: "Enemy ships cannot jump from this system.",
    Shield: 4,
    Type: "Battleship",
    Image: "interdictor.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 32,
    Attack: "B1",
    Cost: "2P",
    Faction: "Sol",
    Hull: 3,
    Name: "Harvester",
    Rules: "Gain +1P per turn while in a system you control.",
    Shield: 3,
    Type: "Frigate",
    Image: "harvester.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 33,
    Attack: "D4",
    Cost: "5P",
    Faction: "Sol",
    Hull: 6,
    Name: "Enforcer",
    Rules: "+1 loyalty for all systems you control within 1 jump.",
    Shield: 6,
    Type: "Battleship",
    Image: "enforcer.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 36,
    Attack: "D3",
    Cost: "5P",
    Faction: "Sol",
    Hull: 8,
    Name: "Protector",
    Rules: "You may redirect any beam or disruptor damage to this ship.",
    Shield: 4,
    Type: "Battleship",
    Image: "protector.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 37,
    Attack: "D2",
    Cost: "3P",
    Faction: "Sol",
    Hull: 4,
    Name: "Railgun Ship",
    Rules: "You may redirect any beam or disruptor damage to this ship.",
    Shield: 3,
    Type: "Cruiser",
    Image: "railgun_ship.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 41,
    Attack: "D4",
    Cost: "5P",
    Faction: "Sol",
    Hull: 6,
    Name: "Conqueror",
    Rules: "-1 loyalty in an enemy system.",
    Shield: 6,
    Type: "Battleship",
    Image: "conqueror.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 49,
    Cost: "3C",
    Faction: "Sol",
    Flavor:
      '"Our agents are everywhere. These rebels have no hope." - Rasik Setenden, SIF Blade\'s Edge',
    Name: "Sabotage",
    Rules: "Destroy target ship or station.",
    Type: "Command",
    Image: "sabotage.jpg"
  },
  {
    ID: 50,
    Cost: "3P",
    Faction: "Sol",
    Flavor:
      '"Our empire is a furnace, taking in the raw resources of the galaxy and bringing about the greatest beauty ever - war." - Rasik Setanden, SIF Blade\'s Edge',
    Name: "Industrial Theory",
    Rules: "Planets you control gain +1P per turn.",
    Type: "Research",
    Image: "industrial_theory.jpg"
  },
  {
    ID: 52,
    Cost: "3P",
    Faction: "Sol",
    Flavor:
      "Medical log, entry 39. Our attempts to curb rebellion have finally proven successful. The new labs are ready to open to encourage our fallen brethren to rejoin civilization.",
    Name: "Rehabilitation Tower",
    Rules: "Planets you control have +1 loyalty.",
    Type: "Research",
    Image: "rehabilitation_tower.jpg"
  },
  {
    ID: 53,
    Credits: 2,
    Faction: "Sol",
    Loyalty: 5,
    Name: "Sol",
    Production: 3,
    Type: "Capital Terran Planet",
    Image: "sol.jpg",
    HandContext: [
      {
        Text: "Explore System",
        Action: "explore"
      }
    ]
  },
  {
    ID: 54,
    Credits: 1,
    Faction: "Sol",
    Loyalty: 3,
    Name: "Loyal Planet",
    Production: 1,
    Type: "Terran Planet",
    Image: "loyal_planet.jpg",
    HandContext: [
      {
        Text: "Explore System",
        Action: "explore"
      }
    ]
  },
  {
    ID: 56,
    Credits: 1,
    Faction: "Sol",
    Loyalty: 2,
    Name: "Production Planet",
    Production: 2,
    Type: "Terran Planet",
    Image: "production_planet.jpg",
    HandContext: [
      {
        Text: "Explore System",
        Action: "explore"
      }
    ]
  },
  {
    ID: 57,
    Attack: "B1",
    Cost: "1P",
    Faction: "Karenlay",
    Flavor:
      '"To search the stars for the truths of life... there is no greater calling." - Commander Karenlay, KAS Thunderous Fury',
    Hull: 2,
    Name: "Seeker",
    Rules: "When this ship enters a system, draw a card.",
    Shield: 2,
    Type: "Frigate",
    Image: "seeker.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 58,
    Attack: "D2",
    Cost: "2P",
    Faction: "Karenlay",
    Hull: 4,
    Name: "Syphonship",
    Rules: "Drain Shield",
    Shield: 4,
    Type: "Frigate",
    Image: "syphonship.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 59,
    Attack: "B1",
    Cost: "2P",
    Faction: "Karenlay",
    Hull: 2,
    Name: "Medic",
    Rules:
      "At any time, you may transfer 1 shield from this ship to another target ship.",
    Shield: 6,
    Type: "Frigate",
    Image: "medic.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 60,
    Attack: "B2",
    Cost: "3P",
    Faction: "Karenlay",
    Hull: 2,
    Name: "Lurker",
    Rules: "Cloak",
    Shield: 4,
    Type: "Frigate",
    Image: "lurker.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 61,
    Attack: "D2",
    Cost: "3P",
    Faction: "Karenlay",
    Hull: 3,
    Name: "Crossbow",
    Rules: "Long Range",
    Shield: 4,
    Type: "Cruiser",
    Image: "crossbow.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 64,
    Attack: "B2",
    Cost: "4P",
    Faction: "Karenlay",
    Hull: 2,
    Name: "Manipulator",
    Rules:
      "1C: Redirect 1 beam or disruptor damage to target ship you control.",
    Shield: 5,
    Type: "Cruiser",
    Image: "manipulator.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 66,
    Attack: "D3",
    Cost: "5P",
    Faction: "Karenlay",
    Hull: 4,
    Name: "Hammershield",
    Rules: "You may redirect any beam or disruptor damage to this ship.",
    Shield: 8,
    Type: "Battleship",
    Image: "hammershield.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 73,
    Cost: "2C",
    Faction: "Karenlay",
    Flavor:
      "What else is finer than that of the universe? Only the stars hold the truths we seek.",
    Name: "Inspiration",
    Rules: "Draw two cards.",
    Type: "Command",
    Image: "inspiration.jpg"
  },
  {
    ID: 76,
    Cost: "1C",
    Faction: "Karenlay",
    Name: "Raise Shields!",
    Rules: "Regenerate 3 shield on target ship.",
    Type: "Maneuver",
    Image: "raise_shields.jpg"
  },
  {
    ID: 77,
    Cost: "2C",
    Faction: "Karenlay",
    Name: "Hyperdrive Malfunction",
    Rules:
      "The next time target ship you don't control jumps, return it to its owner's hand.",
    Type: "Maneuver",
    Image: "hyperdrive_malfunction.jpg"
  },
  {
    ID: 79,
    Credits: 2,
    Faction: "Karenlay",
    Loyalty: 4,
    Name: "Karenlay",
    Production: 2,
    Type: "Capital Ice Planet",
    Image: "karenlay.jpg",
    HandContext: [
      {
        Text: "Explore System",
        Action: "explore"
      }
    ]
  },
  {
    ID: 80,
    Credits: 1,
    Faction: "Karenlay",
    Loyalty: 2,
    Name: "Military Training World",
    Production: 2,
    Type: "Desert Planet",
    Image: "military_training_world.jpg",
    HandContext: [
      {
        Text: "Explore System",
        Action: "explore"
      }
    ]
  },
  {
    ID: 81,
    Credits: 1,
    Faction: "Karenlay",
    Loyalty: 1,
    Name: "Research Planet",
    Production: 1,
    Type: "Ice Planet",
    Image: "research_planet.jpg",
    HandContext: [
      {
        Text: "Explore System",
        Action: "explore"
      }
    ]
  },
  {
    ID: 83,
    Cost: "2P",
    Faction: "Karenlay",
    Name: "Advanced Shields",
    Rules: "Ships you control get +1 shield.",
    Type: "Research",
    Image: "advanced_shields.jpg"
  },
  {
    ID: 85,
    Attack: "B1",
    Cost: "1P",
    Faction: "Forsei",
    Flavor:
      '"I will know my task is complete when the skies are full of ships bearing our name, all of them bringing the wealth of the universe to us." - Gerald Forsei III, Forsein Trade Commission',
    Hull: 2,
    Name: "Freighter",
    Rules: "When this ship enters a system, gain 1C",
    Shield: 2,
    Type: "Frigate",
    Image: "freighter.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 86,
    Attack: "B0",
    Cost: "2P",
    Faction: "Forsei",
    Flavor:
      "The perfect ship for hit and run attacks, it is able to deploy a few fighters and allow them to do the dirty work.",
    Hull: 3,
    Name: "Tri-Carrier",
    Rules: "Carrier 3",
    Shield: 4,
    Type: "Cruiser",
    Image: "tri_carrier.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 89,
    Attack: "M2",
    Cost: "3P",
    Faction: "Forsei",
    Hull: 2,
    Name: "Catapult",
    Rules: "Long Range",
    Shield: 2,
    Type: "Cruiser",
    Image: "catapult.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 92,
    Attack: "B2",
    Cost: "2P",
    Faction: "Forsei",
    Flavor:
      "According to section 15, paragraph 126, line 5, all citizens living on the same planet as a trade port must pay a maintenance fee. Failure to do so will result in a fine.",
    Hull: 3,
    Name: "Tax Collector",
    Rules: "Gain +1C per turn when this ship is in a system you control.",
    Shield: 3,
    Type: "Frigate",
    Image: "tax_collector.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 94,
    Attack: "B3",
    Cost: "4P",
    Faction: "Forsei",
    Flavor:
      '"The galaxy is full of two kinds of people - the victims, and us." - Captain Volnok',
    Hull: 3,
    Name: "Pirate Raider",
    Rules: "Contract 2 | Disable",
    Shield: 3,
    Type: "Frigate",
    Image: "pirate_raider.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 97,
    Attack: "M2",
    Cost: "3P",
    Faction: "Forsei",
    Flavor: '"For the right price, I\'ll shoot anyone." - Captain Volnok',
    Hull: 2,
    Name: "Pirate Sniper",
    Rules: "Contract 1 | Long Range",
    Shield: 2,
    Type: "Frigate",
    Image: "pirate_sniper.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 98,
    Attack: "B1",
    Cost: "5P",
    Faction: "Forsei",
    Flavor:
      "Throughout the Forsein League, caravan ships travel from place to place, essentially worlds of their own, flying among the stars.",
    Hull: 6,
    Name: "Caravan",
    Rules: "Carrier 5, +1 income",
    Shield: 6,
    Type: "Battleship",
    Image: "caravan.jpg",
    HandContext: [
      {
        Text: "Build Ship",
        Action: "build"
      }
    ],
    PlayContext: [
      {
        Text: "Move Ship",
        Action: "move"
      }
    ]
  },
  {
    ID: 101,
    Attack: "B1",
    Cost: "4P",
    Faction: "Forsei",
    Flavor:
      "Trade port governors know how to cut a deal to get the best products to their partners.",
    Hull: 9,
    Name: "Trade Port",
    Rules: "3C: Draw a card | Discard a card: Gain 1C",
    Shield: 9,
    Type: "Station",
    Image: "trade_port.jpg",
    HandContext: [
      {
        Text: "Build Station",
        Action: "build"
      }
    ],
    PlayContext: []
  },
  {
    ID: 108,
    Cost: "6C",
    Faction: "Forsei",
    Name: "Depression",
    Rules:
      "Until your next ready phase, commands and maneuvers cost 4C more, and ships and stations cost 4P more.",
    Type: "Command",
    Image: "depression.jpg"
  },
  {
    ID: 103,
    Cost: "2C",
    Faction: "Forsei",
    Flavor: "Soon, all your secrets will be mine.",
    Name: "Spy Network",
    Rules:
      "Target player reveals their hand. That player discards a card of your choice unless they pay you 3C.",
    Type: "Command",
    Image: "spy_network.jpg"
  },
  {
    ID: 107,
    Cost: "3P",
    Faction: "Forsei",
    Flavor:
      '"Those who value money as something to be gained alone will never understand that it can be used for greater purposes." - Gerald Forsei III, Forsein Trade Commission.',
    Name: "Economic Theory",
    Rules: "Planets you control have +1 income.",
    Type: "Research",
    Image: "economic_theory.jpg"
  },
  {
    ID: 108,
    Cost: "4P",
    Faction: "Forsei",
    Name: "Minefield",
    Rules:
      "Pay 2C: Place a mine in target system. | During the combat phase, if enemy ships are in a system with mines, destroy all mines and deal 1 damage to all enemy ships for each mine destroyed.",
    Type: "Research",
    Image: "minefield.jpg"
  },
  {
    ID: 110,
    Credits: 3,
    Faction: "Forsei",
    Loyalty: 4,
    Name: "Forsei",
    Production: 2,
    Type: "Capital Desert Planet",
    Image: "forsei.jpg",
    HandContext: [
      {
        Text: "Explore System",
        Action: "explore"
      }
    ]
  },
  {
    ID: 111,
    Credits: 2,
    Faction: "Forsei",
    Loyalty: 3,
    Name: "Trade World",
    Production: 1,
    Type: "Gas Planet",
    Image: "trade_world.jpg",
    HandContext: [
      {
        Text: "Explore System",
        Action: "explore"
      }
    ]
  },
  {
    ID: 112,
    Credits: 1,
    Faction: "Forsei",
    Loyalty: 1,
    Name: "Storage Asteroid",
    Production: 1,
    Type: "Barren Planet",
    Image: "storage_asteroid.jpg",
    HandContext: [
      {
        Text: "Explore System",
        Action: "explore"
      }
    ]
  }
];

export default Cards;
