const app = require("express")();
const http = require("http").Server(app);
const io = require("socket.io")(http);
const dgram = require("dgram");

// app.get("/", function(req, res) {
//   res.sendFile(__dirname + "/gameServerClient.html");
// });

// Track game state.
// Client game state is a reflection of this.
// When a client connects, their game state is set to equal this.
let GameState = {
  // Should determine this after both players are connected.
  ActivePlayer: 0,
  ActivePhase: "first",
  ReadyPhaseDone: false,
  CommandPhaseDone: false,
  CombatPhaseDone: false,
  EndPhaseDone: false,
  Player1: {
    ID: 1,
    Name: "",
    DeckLoaded: false,
    Deck: [],
    Discard: [],
    Hand: [],
    Research: [],
    Credits: 0,
    Production: 0
  },
  Player2: {
    ID: 2,
    Name: "",
    DeckLoaded: false,
    Deck: [],
    Discard: [],
    Hand: [],
    Research: [],
    Credits: 0,
    Production: 0
  },
  Systems: [
    {
      ID: 0,
      Card: null,
      ControlledBy: null,
      Player1Fleet: [],
      Player2Fleet: [],
      Style: {}
    },
    {
      ID: 1,
      Card: null,
      ControlledBy: null,
      Player1Fleet: [],
      Player2Fleet: [],
      Style: {}
    },
    {
      ID: 2,
      Card: null,
      ControlledBy: null,
      Player1Fleet: [],
      Player2Fleet: [],
      Style: {}
    },
    {
      ID: 3,
      Card: null,
      ControlledBy: null,
      Player1Fleet: [],
      Player2Fleet: [],
      Style: {}
    },
    {
      ID: 4,
      Card: null,
      ControlledBy: null,
      Player1Fleet: [],
      Player2Fleet: [],
      Style: {}
    },
    {
      ID: 5,
      Card: null,
      ControlledBy: null,
      Player1Fleet: [],
      Player2Fleet: [],
      Style: {}
    },
    {
      ID: 6,
      Card: null,
      ControlledBy: null,
      Player1Fleet: [],
      Player2Fleet: [],
      Style: {}
    },
    {
      ID: 7,
      Card: null,
      ControlledBy: null,
      Player1Fleet: [],
      Player2Fleet: [],
      Style: {}
    },
    {
      ID: 8,
      Card: null,
      ControlledBy: null,
      Player1Fleet: [],
      Player2Fleet: [],
      Style: {}
    }
  ]
};

// Track number of players.
// Only advertise game with less than 2 connections.
let socketConnections = 0;

io.on("connection", socket => {
  socketConnections++;

  // Once both players are connected, determine who goes first
  // and call for initial hands to be drawn.
  // We'll worry about mulligans later.

  io.emit("fetchInitPlayerState", { to: socket.id });
  
  socket.on("chat message", msg => {
    io.emit("chat message", msg);
  });

  socket.on("updateGameState", Update => {
    switch (Update.Where) {
      case "active":
        GameState.ActivePlayer = Update.Data.ActivePlayer;
        GameState.ActivePhase = Update.Data.ActivePhase;
        GameState.ReadyPhaseDone = Update.Data.ReadyPhaseDone;
        GameState.CommandPhaseDone = Update.Data.CommandPhaseDone;
        GameState.CombatPhaseDone = Update.Data.CombatPhaseDone;
        GameState.EndPhaseDone = Update.Data.EndPhaseDone;
        break;
      case "player1":
        if (GameState.Player1.Name != Update.Data.Name) {
          io.emit("chat message", `${Update.Data.Name} connected.`);
        }
        GameState.Player1 = Update.Data;
        break;
      case "player2":
        if (GameState.Player2.Name != Update.Data.Name) {
          io.emit("chat message", `${Update.Data.Name} connected.`);

          if (socketConnections == 2) {
            GameState.ActivePlayer = Math.floor(Math.random() * 2) + 1;
          }
        }
        GameState.Player2 = Update.Data;
        break;
      case "system":
        GameState.Systems = Update.Data;
        break;
    }
    io.emit("syncGameState", GameState);
  });

  socket.on("disconnect", () => {
    socketConnections--;
    console.log("user disconnected");
  });
});

let server = dgram.createSocket("udp4");

server.bind(() => {
  server.setBroadcast(true);
  setInterval(broadcastGame, 3000);
});

let announceGame = true;

const broadcastGame = () => {
  // If there are two players, send a message to remove game from list.
  // If there's one player, add game to list.
  if (socketConnections < 2) {
    let message = new Buffer("displayGame");
    server.send(message, 0, message.length, 3001, "255.255.255.255");
    announceGame = true;
  } else if (socketConnections >= 2 && announceGame === true) {
    let message = new Buffer("removeGame");
    server.send(message, 0, message.length, 3001, "255.255.255.255");
    announceGame = false;
  }
};

http.listen(3000, function() {
  console.log("listening on *:3000");
});
